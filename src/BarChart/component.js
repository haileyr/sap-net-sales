import React, {Component} from 'react';
import * as d3 from 'd3';
import PropTypes from 'prop-types';
import './style.css';

export default class BarChart extends Component {
	static propTypes = {
		data: PropTypes.arrayOf(PropTypes.object),
		className: PropTypes.string,
		height: PropTypes.string,
		width: PropTypes.string,
		title: PropTypes.string
	}

	constructor(props) {
		super(props)
		this.props.data.forEach(quarter => {
			quarter.sales_number = parseInt(quarter.sales.replace(/\D/g, ''), 10);
		})
		this.margin = {top: 20, right: 20, bottom: 30, left: 40}
	}

	componentDidMount() {
		this.width = parseInt(d3.select('#chart').style('width'), 10) - this.margin.left - this.margin.right;
		this.height = parseInt(d3.select('#chart').style('height'), 10) - this.margin.top - this.margin.bottom;
		
		this.x = d3.scaleBand()
			.range([0, this.width])
			.padding(0.1);
		this.y = d3.scaleLinear()
			.range([this.height, 0]);
		this.svg = d3.select('#chart svg')
			.attr('width', this.width + this.margin.left + this.margin.right)
			.attr('height', this.height + this.margin.top + this.margin.bottom)
			.append('g')
			.attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

		this.svg.append('g')
			.attr('class', 'axis x-axis');
		this.svg.append('g')
			.attr('class', 'axis y-axis');

		this.drawChart();
		window.addEventListener("resize", this.resize(this.svg));
	}

	componentWillUpdate(nextProps) {
		this.updateData(nextProps.data)
	}

	drawChart() {
		const {data} = this.props;
		this.x.domain(data.map(d => d.quarter));
		this.y.domain([0, d3.max(data, d => d.sales_number)]);
		this.svg.selectAll('rect')
			.data(data)
			.enter()
			.append('rect')
			.attr('class', 'bar')
			.attr('x', (d, i) => this.x(d.quarter))
			.attr('width', this.x.bandwidth())
			.attr('y', d => this.y(d.sales_number))
			.attr('height', d => this.height - this.y(d.sales_number))
			.attr('fill', 'rgb(0, 143, 211)')

		this.svg.select('.x-axis')
			.attr('transform', 'translate(0,' + this.height + ')')
			.call(d3.axisBottom(this.x));

		this.svg.select('.y-axis')
			.call(d3.axisLeft(this.y));

		this.svg.selectAll('.text')
			.data(data)
			.enter()
			.append('text')
			.attr('class','label')
			.attr('x', (d => this.x(d.quarter) + this.x.bandwidth() / 2   ))
			.attr('y', d => this.y(d.sales_number) + 1)
			.attr('dy', '.75em')
			.text(d => d.sales); 
	}

	updateData(data) {
		const width = parseInt(d3.select('#chart').style('width'), 10) - this.margin.left - this.margin.right

		data.forEach(quarter => {
			quarter.sales_number = parseInt(quarter.sales.replace(/\D/g, ''), 10);
		})
		
		this.x.range([0, width])
			.padding(0.1)
			.domain(this.props.data.map(d => d.quarter));
		this.y.domain([0, d3.max(data, d => d.sales_number)]);
		this.width = parseInt(d3.select('#chart').style('width'), 10) - this.margin.left - this.margin.right
		
		const bars = this.svg.selectAll('.bar')
			.remove()
			.exit()
			.data(data);

		bars.enter()
			.append('rect')
			.attr('class', 'bar')
			.attr('x', (d, i) => this.x(d.quarter))
			.attr('width', this.x.bandwidth())
			.attr('y', d => this.y(d.sales_number))
			.attr('height', d => this.height - this.y(d.sales_number))
			.attr('fill', 'rgb(0, 143, 211)')
		const labels = this.svg.selectAll('.label').remove()
			.exit()
			.data(data);
		labels.enter()
			.append('text')
			.attr('class','label')
			.attr('x', (d => this.x(d.quarter) + this.x.bandwidth() / 2   ))
			.attr('y', d => this.y(d.sales_number) + 1)
			.attr('dy', '.75em')
			.text(d => d.sales);
	}

	resize = () => () => {
		const width = parseInt(d3.select('#chart').style('width'), 10) - this.margin.left - this.margin.right
			
		this.svg = d3.select('#chart svg')
			.attr('width', width + this.margin.left + this.margin.right);

		const x = d3.scaleBand()
			.range([0, width])
			.padding(0.1)
			.domain(this.props.data.map(d => d.quarter));

		this.svg.selectAll('.bar')
			.attr('x', (d, i) => x(d.quarter))
			.attr('width', x.bandwidth())
			.attr('fill', 'rgb(0, 143, 211)')
		this.svg.selectAll('.label').enter()
			.append('text')
			.attr('class','label')
			.attr('x', (d => x(d.quarter) + x.bandwidth() / 2   ))
			.attr('y', d => this.y(d.sales_number) + 1)
			.attr('dy', '.75em')
			.text(d => d.sales); 
		this.svg.select(".x-axis")
		.call(d3.axisBottom(x))
		.attr("transform", "translate(0," + this.height + ")")
		.select(".label")
			.attr("transform", "translate(" + width / 2 + "," + this.margin.bottom / 1.5 + ")");

		const labels = this.svg.selectAll('.label').remove()
			.exit()
			.data(this.props.data);
		labels.enter()
			.append('text')
			.attr('class','label')
			.attr('x', (d => x(d.quarter) + x.bandwidth() / 2   ))
			.attr('y', d => this.y(d.sales_number) + 1)
			.attr('dy', '.75em')
			.text(d => d.sales);
	}

	render() {
		const {className, width, height, title} = this.props;
		return <div className={className}>
			<h3>{title}</h3>
			<div id='chart'>
				<svg width={width} height={height}></svg>
			</div>
		</div>
	}
}