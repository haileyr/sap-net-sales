import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class TopNav extends Component {
	static propTypes = {
		logo: PropTypes.string.isRequired,
		// onClick: PropTypes.func.isRequired,
	}

  render() {
  	const { logo, title } = this.props;

    return (
        <nav>
        	<img src={logo} alt='logo'/>
          <span className='nav-title'>{title}</span>
        </nav>
    );
  }
}
