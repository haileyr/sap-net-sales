import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.css';

const Arrow = ({direction, onClick, glyph, visibility}) => (
	<div
		className={`slide-arrow ${direction}`}
		onClick={onClick}
		style={{
			visibility
		}}>
		{glyph}
	</div>
);

export default class Carousel extends Component {
	static propTypes = {
		updateIndex: PropTypes.func
	}
	constructor(props) {
		super(props);
		this.state = {
			index: 0,
			componentWidth: 0,
			transform: ''
		};
		// assuming we only have 3 components in this slide.
		this.transformMultiply = [1, 0, -1];
	}

	componentWillMount(){
		this.setState({
			componentWidth: window.innerWidth,
			transform: `translateX(${window.innerWidth * 1}px)`
		});
	}

	goPrev = () => {
		this.setState(prevState => {
			const newIndex = prevState.index > -1 ? prevState.index - 1 : prevState.index;
			const translatePx = this.state.componentWidth * this.transformMultiply[newIndex];
			return {
				transform: `translateX(${translatePx}px)`,
				index: newIndex
			}
		}, () => this.props.updateIndex(this.state.index));
	}

	goNext = () => {
		this.setState(prevState => {
			const newIndex = prevState.index < this.props.children.length ? prevState.index + 1 : prevState.index;
			const translatePx = this.state.componentWidth * this.transformMultiply[newIndex];
			return {
				transform: `translateX(${translatePx}px)`,
				index: newIndex
			}
		}, () => this.props.updateIndex(this.state.index));
	}

	render() {
		const { children } = this.props;
		const { index, componentWidth, transform } = this.state;

		return (
			<div>
				<Arrow
					direction="left"
					onClick={this.goPrev}
					glyph="&#9664;"
					visibility={index === 0 ? 'hidden' : 'visible'} />
				<div
					className='carousel-slider'
					style={{
						width: `${componentWidth * children.length}px`,
						transform: transform
					}}
					>
					{
						children.map((child, idx) => {
							return <div
								key={`carousel-child${idx}`}
								className='carousel-component'
								style={{
									width: `${componentWidth}px`,
								}}>
								{child}
							</div>
						})
					}
				</div>
				<Arrow
					direction="right"
					onClick={this.goNext}
					glyph="&#9658;"
					visibility={index === children.length - 1 ? 'hidden' : 'visible'} />
			</div>
		);
	}
}
