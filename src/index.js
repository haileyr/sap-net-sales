import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import TopNav from './TopNav/component';
import CircleButton from './CircleButton/component';
import BarChart from './BarChart/component';
import Table from './Table/component';
import Carousel from './Carousel/component';
import logo from './asset/sap.svg';
import data from './asset/nets-sales.json';

class NetSales extends Component {
	constructor() {
		super()
		this.state = {
			countryIndex: 0
		}
	}

	onClick = (idx) => () => {
		this.setState({countryIndex: idx});
	}

	updateIndex = (idx) => {
		this.setState(prevState => {
			return {countryIndex: idx};
		})
	}

	render() {
		const { countryIndex } = this.state;

		return <div className='net-sales'>
			<TopNav logo={logo} title='Net Sales' />
			<div className='mobile_false btn-list'>
				{
					data.map((country, idx) => {
						return <CircleButton
							key={idx}
							title={country.name}
							subTitle={country.country_data.net_sales}
							onClick={this.onClick(idx)}
							condition={countryIndex === idx}/>
					})
				}
			</div>
			<div className='mobile_true btn-list'>
				<Carousel
					updateIndex={this.updateIndex}
				>
					{
						data.map((country, idx) => {
							return <CircleButton
								key={idx}
								title={country.name}
								subTitle={country.country_data.net_sales}
								onClick={this.onClick(idx)}
								condition={countryIndex === idx}/>
						})
					}
				</Carousel>
			</div>
			<BarChart
				className='chart-container'
				title={`Net Sales for ${data[countryIndex].name}`}
				data={data[countryIndex].country_data.yearly_distribution}/>
			<Table
				className='table-container'
				title='Best Seller Products'
				data={data[countryIndex].country_data.best_sellers}/>
			</div>;
	}
}

ReactDOM.render(
	<div>
		<NetSales />
	</div>,
	document.getElementById('root')
);
