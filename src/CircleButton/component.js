import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class CircleButton extends Component {
	static propTypes = {
		title: PropTypes.string.isRequired,
		onClick: PropTypes.func,
    condition: PropTypes.bool,
    subTitle: PropTypes.string
	}

  render() {
  	const { title, subTitle, condition, onClick } = this.props;

    return (
        <button
        	className={`cirle-button on__${condition}`}
        	onClick={onClick} >
        	<p className={'title'}>{title}</p>
          <p className={'sub-title'}>{subTitle}</p>
        </button>
    );
  }
}
