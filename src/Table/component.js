import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './style.css';

export default class Table extends Component {
	static propTypes = {
		title: PropTypes.string.isRequired,
		data: PropTypes.arrayOf(PropTypes.object).isRequired,
    className: PropTypes.string
	}

  render() {
  	const { title, data, className } = this.props;
    const headers = Object.keys(data[0]);

    return <div className={className}>
        <h3>{title}</h3>
        <table>
          <tbody>
          <tr>{ 
              headers.map(header => {
                return <th key={header}>{header.charAt(0).toUpperCase() + header.slice(1)}</th>
              })
            
          }</tr>
          {
            data.map(product => {
              return <tr key={product.name}>
                {
                  Object.keys(product).map(key => <td key={product[key]}>{product[key]}</td>)
                }
              </tr>
            })
          }</tbody>
        </table>
    </div>;
  }
}
