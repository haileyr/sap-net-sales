## How to
1. Clone the repo.
2. npm install
3. npm run start

## Known issue
- After resizing, when updating data, chart placement fails.
